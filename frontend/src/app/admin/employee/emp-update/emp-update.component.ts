import { ActivatedRoute, Router } from '@angular/router';
import { AdminService } from './../../admin.service';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-emp-update',
  templateUrl: './emp-update.component.html',
  styleUrls: ['./emp-update.component.css']
})
export class EmpUpdateComponent implements OnInit {

  empData: any = {};
  departmentList: any = [];
  isLoading: boolean = false;

  constructor(private adminService: AdminService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.isLoading = true;
    this.empData.username = this.route.snapshot.queryParamMap.get('username');
    this.empData.name = this.route.snapshot.queryParamMap.get('name');
    this.empData.age = this.route.snapshot.queryParamMap.get('age');
    this.empData.department = this.route.snapshot.queryParamMap.get('department');

    this.employeeUpdateForm.patchValue({
      username: this.empData.username,
      firstname: this.empData.name.split(' ')[0],
      lastname: this.empData.name.split(' ')[1],
      age: this.empData.age,
      password: ''
    });

    this.adminService.getDepartment().subscribe(res => {
      if (typeof res.message == 'object') {
        this.departmentList = res.message;
      }
    });
    this.isLoading = false;
  }

  employeeUpdateForm = new FormGroup({
    username: new FormControl('', Validators.required),
    firstname: new FormControl('', Validators.required),
    lastname: new FormControl('', Validators.required),
    age: new FormControl('', Validators.required),
    password: new FormControl(''),
    departmentname: new FormControl('')
  });

  onUpdate() {
    let data = this.employeeUpdateForm.value;
    if (!this.employeeUpdateForm.value.password) {
      delete data.password;
    }
    if (!this.employeeUpdateForm.value.departmentname) {
      delete data.departmentname;
    }
    this.adminService.updateEmployee(data).subscribe(res => {
      this.router.navigate(['/admin/empList']);
    });
  }

  onClickBack() {
    this.router.navigate(['/admin/empList']);
  }

  onClickHome() {
    this.router.navigate(['/admin']);
  }
}
