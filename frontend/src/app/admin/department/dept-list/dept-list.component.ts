import { Router } from '@angular/router';
import { AdminService } from './../../admin.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dept-list',
  templateUrl: './dept-list.component.html',
  styleUrls: ['./dept-list.component.css']
})
export class DeptListComponent implements OnInit {

  departmentList: any;
  displayedColumns: any;
  showDelete: boolean = false;
  showDept: boolean = false;
  isLoading: boolean = false;
  deptName: string = '';

  constructor(private adminService: AdminService, private router: Router ) { }

  ngOnInit(): void {
    this.isLoading = true;
    this.adminService.getDepartment().subscribe(res => {
      if (typeof res.message == 'object') {
        this.showDept = true;
        this.displayedColumns = ['no', 'Department', 'edits'];
        let departmentList = res.message;
        for (let i = 0; i < departmentList.length; i++) {
          departmentList[i] = {
            department_name: departmentList[i].department_name,
            no: i + 1
          }
        }
        this.departmentList = departmentList;
        this.isLoading = false;
      } else {
        this.isLoading = false;
      }
    });
  }

  onUpdate(deptName: string) {
    this.router.navigate(['/admin/updateDept'], { queryParams: { departmentname: deptName } });
  }

  onDelete(deptName: string) {
    this.deptName = deptName;
    this.showDelete = true;
  }

  onClickDelete() {
    this.isLoading = true;
    this.adminService.deleteDepartment(this.deptName).subscribe(res => {
      this.deptName = '';
      this.isLoading = false;
      window.alert(res.message)
      this.router.navigate(['/admin']);
    });
  }

  onClickHide() {
    this.showDelete = false
  }

  onClickBack() {
    this.router.navigate(['/admin']);
  }
}
