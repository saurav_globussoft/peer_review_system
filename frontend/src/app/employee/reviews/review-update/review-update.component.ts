import { EmployeeService } from './../../employee.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-review-update',
  templateUrl: './review-update.component.html',
  styleUrls: ['./review-update.component.css']
})
export class ReviewUpdateComponent implements OnInit {

  username: any;
  score: any;
  comment: any;
  isLoading: boolean = false;
  scoreValArr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

  constructor(private route: ActivatedRoute, private router: Router, private empService: EmployeeService) { }

  ngOnInit(): void {
    this.isLoading = true;
    this.username = this.route.snapshot.queryParamMap.get('username');
    this.score = this.route.snapshot.queryParamMap.get('score');
    this.comment = this.route.snapshot.queryParamMap.get('comment');

    this.reviewUpdateForm.patchValue({
      username: this.username,
      score: this.score,
      comment: this.comment
    });
    this.isLoading = false;
  }

  reviewUpdateForm = new FormGroup({
    username: new FormControl(''),
    score: new FormControl('', Validators.required),
    comment: new FormControl('', { validators: [Validators.required, Validators.minLength(3)] }),
  });

  onUpdate() {
    if (!this.reviewUpdateForm.valid) {
      return;
    }
    this.empService.updateReview(this.reviewUpdateForm.value).subscribe(res => {
      this.router.navigate(['/employee/review']);
    });
  }

  onReset() {
    this.reviewUpdateForm.patchValue({
      username: this.username,
      score: this.score,
      comment: this.comment
    });
  }

  onClickBack() {
    this.router.navigate(['/employee/review']);
  }
}
