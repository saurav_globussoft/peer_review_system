import { ActivatedRoute, Router } from '@angular/router';
import { AdminService } from './../../admin.service';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-dept-update',
  templateUrl: './dept-update.component.html',
  styleUrls: ['./dept-update.component.css']
})
export class DeptUpdateComponent implements OnInit {

  departmentname: any = '';
  departmentList: any = [];
  isLoading: boolean = false
  constructor(private adminService: AdminService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.isLoading = true;
    this.departmentname = this.route.snapshot.queryParamMap.get('departmentname');

    this.deptUpdateForm.patchValue({
      departmentname1: this.departmentname
    });

    this.adminService.getDepartment().subscribe(res => {
      this.departmentList = res.message;

      this.departmentList.forEach((element: any) => {
        if (element.department_name == this.departmentname) {
          const index: number = this.departmentList.indexOf(element);
          if (index > -1) {
            this.departmentList.splice(index, 1);
          }
        }
      });
      this.isLoading = false;
    });
  }

  deptUpdateForm = new FormGroup({
    departmentname1: new FormControl('', Validators.required),
    departmentname2: new FormControl('', Validators.required),
    departmentname3: new FormControl('')
  })

  onUpdate() {
    if (!this.deptUpdateForm.value.departmentname2) {
      return;
    }
    delete this.deptUpdateForm.value.departmentname3;
    const data = this.deptUpdateForm.value;
    this.adminService.updateDepartment(data).subscribe(res => {
      this.router.navigate(['/admin/deptList']);
    });
  }

  onClickBack() {
    this.router.navigate(['/admin/deptList']);
  }

  onClickHome() {
    this.router.navigate(['/admin']);
  }

}
