import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EmployeeRoutingModule } from './employee-routing.module';
import { EmpHomeComponent } from './emp-home/emp-home.component';
import { AngularMaterialModule } from '../angular-material.module';
import { EmployeeDetailComponent } from './employee-detail/employee-detail.component';
import { ReviewListComponent } from './reviews/review-list/review-list.component';
import { ReviewGivelistComponent } from './reviews/review-givelist/review-givelist.component';
import { ReviewCreateComponent } from './reviews/review-create/review-create.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ReviewUpdateComponent } from './reviews/review-update/review-update.component';


@NgModule({
  declarations: [
    EmpHomeComponent,
    EmployeeDetailComponent,
    ReviewListComponent,
    ReviewGivelistComponent,
    ReviewCreateComponent,
    ReviewUpdateComponent
  ],
  imports: [
    CommonModule,
    EmployeeRoutingModule,
    AngularMaterialModule,
    ReactiveFormsModule
  ],
  exports: [
    EmpHomeComponent
  ]
})
export class EmployeeModule { }
