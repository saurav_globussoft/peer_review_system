import { Router } from '@angular/router';
import { AdminService } from './../../admin.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-emp-list',
  templateUrl: './emp-list.component.html',
  styleUrls: ['./emp-list.component.css']
})
export class EmpListComponent implements OnInit {

  employeeList: any;
  displayedColumns: any;
  showDelete: boolean = false;
  username: string = '';
  isLoading: boolean = false;
  showEmp: boolean = false;

  constructor(private adminService: AdminService, private router: Router) { }

  ngOnInit(): void {
    this.isLoading = true;
    this.adminService.getEmployee().subscribe(res => {
      if (typeof res.message == 'object') {
        this.showEmp = true;
        this.displayedColumns = ['no', 'username', 'name', 'age', 'department', 'edits']
        let employeeList = res.message;
        for (let i = 0; i < employeeList.length; i++) {
          if (!employeeList[i].departmentName) {
            employeeList[i].departmentName = '-------';
          }
          employeeList[i] = {
            no: i + 1,
            username: employeeList[i].username,
            name: employeeList[i].name,
            department: employeeList[i].departmentName,
            age: employeeList[i].age
          }
        }
        this.employeeList = employeeList;
        this.isLoading = false;
      } else {
        this.isLoading = false;
      }

    });
  }

  onUpdate(username: string, name: string, department: string, age: number) {
    this.router.navigate(
      ['/admin/updateEmp'],
      {
        queryParams: {
          username: username,
          name: name,
          department: department,
          age: age
        }
      });
  }

  onDelete(username: string) {
    this.username = username;
    this.showDelete = true;
  }

  onClickDelete() {
    this.isLoading = true;
    this.adminService.deleteEmployee(this.username).subscribe(res => {
      this.username = '';
      this.isLoading = false;
      window.alert(res.message);
      this.router.navigate(['/admin']);
    }, err => {
      this.isLoading = false;
      this.router.navigate(['/admin']);
    });
  }

  onClickHide() {
    this.showDelete = false
  }

  onClickBack() {
    this.router.navigate(['/admin']);
  }

}
