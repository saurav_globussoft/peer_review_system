-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.19-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for employee_review_system
CREATE DATABASE IF NOT EXISTS `employee_review_system` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `employee_review_system`;

-- Dumping structure for table employee_review_system.departments
CREATE TABLE IF NOT EXISTS `departments` (
  `department_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `department_name` varchar(30) DEFAULT NULL,
  `admin_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`department_id`),
  KEY `admin_id` (`admin_id`),
  CONSTRAINT `departments_ibfk_1` FOREIGN KEY (`admin_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table employee_review_system.departments: ~9 rows (approximately)
/*!40000 ALTER TABLE `departments` DISABLE KEYS */;
INSERT INTO `departments` (`department_id`, `department_name`, `admin_id`, `created_at`, `updated_at`) VALUES
	(42, 'php', 56, '2021-07-15 19:02:33', '2021-07-15 19:02:33'),
	(44, 'nodejs', 63, '2021-07-19 17:03:20', '2021-07-19 17:03:20'),
	(46, 'ios', 63, '2021-07-24 10:38:39', '2021-07-24 10:38:39'),
	(47, 'android', 63, '2021-07-24 10:38:56', '2021-07-24 10:38:56'),
	(57, 'node', 85, '2021-10-28 13:52:33', '2021-10-28 13:52:33'),
	(58, 'php', 85, '2021-10-28 13:52:51', '2021-10-28 13:52:51'),
	(59, 'android', 85, '2021-10-28 13:53:02', '2021-10-28 13:53:02'),
	(61, 'python', 63, '2021-10-28 14:35:30', '2021-10-28 19:09:57'),
	(62, 'php', 63, '2021-10-28 18:01:17', '2021-10-28 18:01:17');
/*!40000 ALTER TABLE `departments` ENABLE KEYS */;

-- Dumping structure for table employee_review_system.reviews
CREATE TABLE IF NOT EXISTS `reviews` (
  `review_id` int(11) NOT NULL AUTO_INCREMENT,
  `reviewer_id` int(11) DEFAULT NULL,
  `reviewee_id` int(11) DEFAULT NULL,
  `score` int(11) DEFAULT NULL,
  `comments` varchar(1024) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`review_id`),
  KEY `reviewee_id` (`reviewee_id`),
  KEY `reviewer_id` (`reviewer_id`),
  CONSTRAINT `reviews_ibfk_1` FOREIGN KEY (`reviewee_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE,
  CONSTRAINT `reviews_ibfk_2` FOREIGN KEY (`reviewer_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table employee_review_system.reviews: ~8 rows (approximately)
/*!40000 ALTER TABLE `reviews` DISABLE KEYS */;
INSERT INTO `reviews` (`review_id`, `reviewer_id`, `reviewee_id`, `score`, `comments`, `created_at`, `updated_at`) VALUES
	(3, 66, 60, 4, 'slow', '2021-07-19 17:25:51', '2021-07-19 17:25:51'),
	(5, 64, 57, 4, 'slow', '2021-07-24 10:42:20', '2021-07-24 10:42:20'),
	(6, 64, 70, 6, 'slow', '2021-07-24 10:43:06', '2021-07-24 10:43:06'),
	(7, 66, 68, 8, 'good', '2021-07-24 10:46:20', '2021-07-24 10:46:20'),
	(8, 66, 69, 8, 'he is good', '2021-07-24 10:46:50', '2021-07-24 10:46:50'),
	(9, 69, 68, 8, 'he is good', '2021-07-24 10:48:44', '2021-07-24 10:48:44'),
	(10, 69, 60, 10, 'he is good', '2021-07-24 10:49:07', '2021-07-24 10:49:07'),
	(11, 69, 66, 5, 'he is good', '2021-07-24 10:49:18', '2021-07-24 10:49:18');
/*!40000 ALTER TABLE `reviews` ENABLE KEYS */;

-- Dumping structure for table employee_review_system.users
CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `first_name` varchar(30) DEFAULT NULL,
  `last_name` varchar(30) DEFAULT NULL,
  `password` varchar(1024) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `role` varchar(30) DEFAULT NULL,
  `admin_id` int(11) DEFAULT NULL,
  `department_id` int(11) unsigned DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`user_id`),
  KEY `admin_id` (`admin_id`),
  KEY `users_ibfk_2` (`department_id`),
  CONSTRAINT `users_ibfk_2` FOREIGN KEY (`department_id`) REFERENCES `departments` (`department_id`) ON DELETE SET NULL,
  CONSTRAINT `users_ibfk_3` FOREIGN KEY (`admin_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=95 DEFAULT CHARSET=utf8mb4;

-- Dumping data for table employee_review_system.users: ~20 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`user_id`, `username`, `first_name`, `last_name`, `password`, `age`, `role`, `admin_id`, `department_id`, `created_at`, `updated_at`) VALUES
	(56, 'somesh123', 'somesh', 'verma', '$2b$10$t.SN3vgSdK2NhS0BnI/eyOwjD1I4JzbsFvVKjoxHijqspAqnrhNmC', 26, 'admin', NULL, NULL, '2021-07-15 19:01:38', '2021-07-15 19:01:38'),
	(57, 'shatru123', 'shatru', 'kumar', '$2b$10$HfAwZXpV8fBSNiWv71CI6unPXoQYVRB.UegUu8JlkZQL5xAIRUCoS', 23, 'employee', 56, NULL, '2021-07-15 19:02:21', '2021-07-15 19:02:21'),
	(60, 'abc123', 'abc', 'kumar', '$2b$10$cKb5EzhPTyZtcpvFq5gMr.xC4jILZ6LNXMw0fyk0pEBFPQ4Dlv1jW', 23, 'employee', 56, 42, '2021-07-15 19:05:39', '2021-07-15 19:08:24'),
	(63, 'saurav123', 'saurav', 'vishal', '$2b$10$Q9xxbNx8CeOlcy5PnAMn6.HtBsyvOs2uZPb8PQyeAXEzFdD8c7q7S', 25, 'admin', NULL, NULL, '2021-07-19 17:02:36', '2021-07-19 17:02:36'),
	(64, 'sam123', 'sam', 'kumar', '$2b$10$TmcnB50wyv0iO7hgi189Depy7r//4UMs4rg04O.27R/in9hS7srAO', 23, 'employee', 63, 46, '2021-07-19 17:09:15', '2021-10-28 17:54:00'),
	(65, 'shruti123', 'shruti', 'kumar', '$2b$10$WTXw8HTzNpjfpeBSkPB4BemC4rrXRmEDxIYWqcY8OLiZ/Mk0xYwbK', 25, 'employee', 63, 61, '2021-07-19 17:09:45', '2021-10-28 17:38:08'),
	(66, 'xyz123', 'xyz', 'kumar', '$2b$10$se0s9Z0S2rrIcLZC41514.E7XwLP11XZ.LkhYNR/X.qK3gC1PHCcC', 23, 'employee', 63, NULL, '2021-07-19 17:10:22', '2021-07-19 17:12:09'),
	(67, 'avi123', 'avi', 'kumar', '$2b$10$uOJgfNNTC1k3Wmadz1fhoObfwL5KNj9RjCxx/RREKDO8vrarJWVRq', 23, 'employee', 63, 44, '2021-07-24 10:38:08', '2021-07-24 10:38:08'),
	(68, 'avinash123', 'avinash', 'kumar', '$2b$10$eDKAhweqWX.xHPdjh7Bi8.98NI7kN3dSVt4tjztd9G1HpZF7kJIr2', 23, 'employee', 63, NULL, '2021-07-24 10:39:28', '2021-07-24 10:39:28'),
	(69, 'suresh123', 'suresh', 'kumar', '$2b$10$IwdnN3RDB.xf30IxqljIhuOg8cYXCVwmknsGYWG82y2Y3gA/sfxsC', 23, 'employee', 63, NULL, '2021-07-24 10:39:44', '2021-07-24 10:39:44'),
	(70, 'kamal123', 'kamal', 'kumar', '$2b$10$GIo96YcGJv0XT2Jzh5OE5Oq.OnLekOb6MAuc2wkJ7T7lL2V.qRaCG', 23, 'employee', 63, NULL, '2021-07-24 10:40:22', '2021-07-24 10:40:22'),
	(84, 'aka123', 'akash', 'sharma', '$2b$10$T3guHZ4Q75/77fDVh1YzIuL9PsUCbB16.vzFtrRT.PRC9Y0dXfZW2', 25, 'admin', NULL, NULL, '2021-10-27 15:43:10', '2021-10-27 15:43:10'),
	(85, 'amit123', 'amit', 'verma', '$2b$10$m9/OneRZ31t6Kb6VbbGHs.6xF8wMYaidXVsz3E6Qx.nLNB1q0edaW', 25, 'admin', NULL, NULL, '2021-10-27 16:15:13', '2021-10-28 14:26:15'),
	(87, 'samir123', 'samir', 'raj', '$2b$10$LOz2Z51Cwt5nlDpkFcyi..CmhsqdOO1f9A20cVubwMmbap/kLAnFe', 27, 'employee', 85, NULL, '2021-10-28 10:24:28', '2021-10-28 10:24:28'),
	(89, 'ram123', 'ram', 'rahim', '$2b$10$Cjv/8Fp8QRuD6YeppRKtVOLtKDAB07i4SG3/pSCR0VLh4jZaNX0XK', 32, 'employee', 85, 58, '2021-10-28 14:12:11', '2021-10-28 14:12:11'),
	(90, 'sobha123', 'sobha', 'raj', '$2b$10$s4ZGVPeX6RZkpasmK5AxeeKzpuzOd1l/vCK2w8ElNJnRLltJ0bg/.', 23, 'employee', 85, 57, '2021-10-28 14:14:17', '2021-10-28 14:14:17'),
	(91, 'virat123', 'virat', 'kohli', '$2b$10$OsCudofoJHn4C7NMzZOn5.c/v/o4tDyQnCTsIJEz5s.REeAOWW8c2', 32, 'employee', 85, 57, '2021-10-28 14:14:59', '2021-10-28 14:14:59'),
	(92, 'rohit123', 'rohit', 'sharma', '$2b$10$J0RtRYjiCBQu5naH26kbGOPzjfuKYpIyzmK4pnTcbEknwQUDGyfaW', 27, 'employee', 85, 58, '2021-10-28 14:15:40', '2021-10-28 14:15:40'),
	(93, 'varun123', 'varun', 'dhawan', '$2b$10$VvlzguXHbX0M8VXybXNnIO4LWj131y6eFRAt9fs/SXgJE5MD/IsFi', 27, 'employee', 85, 59, '2021-10-28 14:16:22', '2021-10-28 14:16:22'),
	(94, 'siddhart123', 'siddhart', 'malhotra', '$2b$10$3FgMaCWd5nATNLftL8TyQ.1Lo1IZkXRuvEn7Q/x1oD2EzvU0tHJWm', 29, 'employee', 85, NULL, '2021-10-28 14:17:20', '2021-10-28 14:17:20');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
