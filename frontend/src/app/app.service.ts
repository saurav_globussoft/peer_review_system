import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from './login/login.model';
import { Router } from '@angular/router';
import { Subject, Observable } from 'rxjs';

const URL = 'http://localhost:3000';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  private authStatusListener = new Subject<boolean>();
  isAuthenticated: boolean = false;

  constructor(private http: HttpClient, private router: Router) { }

  getAuth() {
    return this.isAuthenticated;
  }

  getAuthStatusListener() {
    return this.authStatusListener.asObservable();
  }

  private saveUser(token: string, role: string, name: string) {
    localStorage.setItem('token', token);
    localStorage.setItem('role', role);
    localStorage.setItem('name', name);
  }

  getRole() {
    const role = localStorage.getItem('role');
    return role;
  }

  getName() {
    const name = localStorage.getItem('name') || '';
    return name;
  }

  getToken() {
    const token = localStorage.getItem('token');
    return token;
  }

  private removeUser() {
    localStorage.removeItem('token');
    localStorage.removeItem('role');
    localStorage.removeItem('name');
  }

  //login for user
  userLogin(username: string, password: string) {
    const authData: User = { username: username, password: password };
    this.http.post<{ code: number, token: string, role: string, name: string }>(`${URL}/login`, authData)
      .subscribe(res => {
        const token = res.token;
        const role = res.role;
        const name = res.name;
        if (token) {
          this.isAuthenticated = true;
          this.authStatusListener.next(true);
          this.saveUser(token, role, name);
          this.homepageRedirect();
        }
      }, err => {
        this.isAuthenticated = false;
        this.authStatusListener.next(false);
        this.router.navigate(['/login']);
      });
  }

  getUserInfo(): Observable<any> {
    return this.http.get(`${URL}/getUserDetail`);
  }

  homepageRedirect() {
    const role = this.getRole();
    if (role === 'admin') {
      this.isAuthenticated = true;
      this.authStatusListener.next(true);
      this.router.navigate(['/admin']);
    } else if ((role === 'employee')) {
      this.isAuthenticated = true;
      this.authStatusListener.next(true);
      this.router.navigate(['/employee']);
    } else {
      this.isAuthenticated = false;
      this.authStatusListener.next(false);
    }
  }

  autoAuthUser() {
    const token = this.getToken();
    if (!token) {
      return;
    }
    this.isAuthenticated = true;
    this.authStatusListener.next(true);
  }

  userLogout() {
    this.isAuthenticated = false;
    this.authStatusListener.next(false);
    this.removeUser();
    this.router.navigate(['/']);
  }
}
