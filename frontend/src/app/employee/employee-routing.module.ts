import { ReviewUpdateComponent } from './reviews/review-update/review-update.component';
import { ReviewGivelistComponent } from './reviews/review-givelist/review-givelist.component';
import { EmployeeDetailComponent } from './employee-detail/employee-detail.component';
import { EmpHomeComponent } from './emp-home/emp-home.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReviewListComponent } from './reviews/review-list/review-list.component';
import { ReviewCreateComponent } from './reviews/review-create/review-create.component';

const routes: Routes = [
  {
    path: '',
    component: EmpHomeComponent
  },
  {
    path: 'details',
    component: EmployeeDetailComponent
  },
  {
    path: 'review',
    component: ReviewListComponent
  },
  {
    path: 'review/review-list',
    component: ReviewGivelistComponent
  },
  {
    path: 'review/create',
    component: ReviewCreateComponent
  },
  {
    path: 'review/update',
    component: ReviewUpdateComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmployeeRoutingModule { }
