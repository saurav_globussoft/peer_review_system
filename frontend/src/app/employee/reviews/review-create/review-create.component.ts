import { EmployeeService } from './../../employee.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-review-create',
  templateUrl: './review-create.component.html',
  styleUrls: ['./review-create.component.css']
})
export class ReviewCreateComponent implements OnInit {

  username: any;
  isLoading: boolean = false;
  scoreValArr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

  constructor(private route: ActivatedRoute, private router: Router, private empService: EmployeeService) { }

  ngOnInit(): void {
    this.isLoading = true;
    this.username = this.route.snapshot.queryParamMap.get('username');
    this.reviewForm.patchValue({
      username: this.username
    });
    this.isLoading = false;
  }

  reviewForm = new FormGroup({
    username: new FormControl('', { validators: [Validators.required, Validators.minLength(3)] }),
    score: new FormControl('', Validators.required),
    comment: new FormControl('', { validators: [Validators.required, Validators.minLength(3)] }),
  });


  onCreate() {
    if (this.reviewForm.invalid) {
      return;
    }
    this.isLoading = true;
    this.empService.createReview(this.reviewForm.value).subscribe(res => {
      this.isLoading = false;
      this.router.navigate(['/employee/review'])
    });
  }

  onReset() {
    this.reviewForm.patchValue({
      username: this.username,
      score: '',
      comment: ''
    });
  }

  onClickBack() {
    this.router.navigate(['/employee/review/review-list']);
  }
}
