import { EmpUpdateComponent } from './employee/emp-update/emp-update.component';
import { RegisterComponent } from './register/register.component';
import { AdminUpdateComponent } from './admin-update/admin-update.component';
import { AdminDetailComponent } from './admin-detail/admin-detail.component';
import { AdminHomeComponent } from './admin-home/admin-home.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateDeptComponent } from './department/create-dept/create-dept.component';
import { DeptListComponent } from './department/dept-list/dept-list.component';
import { DeptUpdateComponent } from './department/dept-update/dept-update.component';
import { EmpListComponent } from './employee/emp-list/emp-list.component';

const routes: Routes = [
  {
    path: '',
    component: AdminHomeComponent
  },
  {
    path: 'details',
    component: AdminDetailComponent
  },
  {
    path: 'update',
    component: AdminUpdateComponent
  },
  {
    path: 'register',
    component: RegisterComponent
  },
  {
    path: 'createDept',
    component: CreateDeptComponent
  },
  {
    path: 'deptList',
    component: DeptListComponent
  },
  {
    path: 'updateDept',
    component: DeptUpdateComponent
  },
  {
    path: 'empList',
    component: EmpListComponent
  },
  {
    path: 'updateEmp',
    component: EmpUpdateComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
