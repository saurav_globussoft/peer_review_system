import { AdminService } from './../admin.service';
import { Router } from '@angular/router';
import { AppService } from './../../app.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms'

@Component({
  selector: 'app-admin-update',
  templateUrl: './admin-update.component.html',
  styleUrls: ['./admin-update.component.css']
})
export class AdminUpdateComponent implements OnInit {

  userData: any;
  data: any;
  isLoading: boolean = false;

  constructor(private appService: AppService, private router: Router, private adminService: AdminService) { }

  ngOnInit(): void {
    this.isLoading = true;
    this.appService.getUserInfo().subscribe(res => {
      this.userData = res;
      this.adminUpdateForm.patchValue({
        username: this.userData.username,
        firstname: this.userData.name.split(' ')[0],
        lastname: this.userData.name.split(' ')[1],
        age: this.userData.age
      })
      this.isLoading = false;
    })
  }

  adminUpdateForm = new FormGroup({
    username: new FormControl('', Validators.required),
    firstname: new FormControl('', Validators.required),
    lastname: new FormControl('', Validators.required),
    age: new FormControl('', Validators.required),
    password: new FormControl('')
  });

  onClickHome() {
    this.router.navigate(['/admin']);
  }

  onClickUpdate() {
    if (!this.adminUpdateForm.valid) {
      return;
    }
    this.isLoading = true;
    this.data = {};
    if (!this.adminUpdateForm.value.password) {
      this.data.username = this.adminUpdateForm.value.username;
      this.data.firstname = this.adminUpdateForm.value.firstname;
      this.data.lastname = this.adminUpdateForm.value.lastname;
      this.data.age = this.adminUpdateForm.value.age;
    } else {
      this.data = this.adminUpdateForm.value;
    }
    this.adminService.updateAdmin(this.data).subscribe(res => {
      localStorage.setItem('name', `${this.data.firstname} ${this.data.lastname}`);
      this.router.navigate(['/admin']);
    });
  }

}
