const mysql = require('mysql');

function mysqlConnection() {
    const connection = mysql.createConnection({
        host: '127.0.0.1',
        user: 'root',
        password: process.env.DBPASS || '',
        database: 'employee_review_system'
    });

    connection.connect((err) => {
        if (err) {
            console.log('error connecting: ' + err.stack);
            return;
        }
        console.log('connected as id ' + connection.threadId);
    });
    return connection;
}

module.exports = mysqlConnection;