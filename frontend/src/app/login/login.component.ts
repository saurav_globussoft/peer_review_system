import { AppService } from './../app.service';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  isLoading: boolean = false;
  token: any = '';
  constructor(private appService: AppService) { }

  ngOnInit(): void {
    this.isLoading = true;
    this.token = this.appService.getToken();
    if (this.token) {
      this.appService.homepageRedirect();
    }
    this.isLoading = false;
  }

  onLogin(form: NgForm) {
    if (form.invalid) {
      return;
    }
    this.appService.userLogin(form.value.username, form.value.password);
  }

}
