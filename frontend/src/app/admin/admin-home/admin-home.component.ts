import { AdminService } from './../admin.service';
import { AppService } from './../../app.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin-home',
  templateUrl: './admin-home.component.html',
  styleUrls: ['./admin-home.component.css']
})
export class AdminHomeComponent implements OnInit {

  fullname: string = '';
  isLoading: boolean = false;
  showDelete: boolean = false;

  constructor(private appServie: AppService, private router: Router, private adminService: AdminService) { }

  ngOnInit(): void {
    this.isLoading = true;
    this.fullname = this.appServie.getName();
    this.isLoading = false;
  }

  onClickLogout() {
    this.appServie.userLogout();
  }

  onClickShowDelete() {
    this.showDelete = true;
  }

  onClickHide() {
    this.showDelete = false;
  }

  onClickDelete() {
    this.adminService.deleteAdmin();
  }
}

