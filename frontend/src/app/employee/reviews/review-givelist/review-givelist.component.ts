import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../../employee.service';

@Component({
  selector: 'app-review-givelist',
  templateUrl: './review-givelist.component.html',
  styleUrls: ['./review-givelist.component.css']
})
export class ReviewGivelistComponent implements OnInit {

  revieweeList: any = [];
  revieweeListArr: any = [];
  reviewsGiven: any;
  isLoading: boolean = false;
  displayedColumns: any;
  showTable: boolean = false;

  constructor(private empService: EmployeeService, private router: Router) { }

  ngOnInit(): void {
    this.isLoading = true;
    this.displayedColumns = ['no', 'username', 'btn'];

    this.empService.getReviewToWhom().subscribe(res => {
      let revieweeList = res.message;

      this.empService.getReviews().subscribe(res => {
        this.reviewsGiven = res.reviewsGiven;
        for (let i = 0; i < this.reviewsGiven.length; i++) {
          this.revieweeListArr.push(this.reviewsGiven[i].giveReviewTo)
        }

        revieweeList = revieweeList.filter((elem: any) => {
          return !this.revieweeListArr.includes(elem)
        });

        for (let i = 0; i < revieweeList.length; i++) {
          this.revieweeList.push({
            username: revieweeList[i],
            no: i + 1
          })
        }

        if (this.revieweeList.length) {
          this.showTable = true;
        }

        this.isLoading = false;
      })
    });
  }

  onReview(username: string) {
    this.router.navigate(['/employee/review/create'],
      { queryParams: { username: username } });
  }

  onClickBack() {
    this.router.navigate(['/employee/review'])
  }
}
