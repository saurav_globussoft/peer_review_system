import { AppService } from './../../app.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-emp-home',
  templateUrl: './emp-home.component.html',
  styleUrls: ['./emp-home.component.css']
})
export class EmpHomeComponent implements OnInit {

  fullname: string = '';

  constructor(private appServie: AppService, private router: Router) { }

  ngOnInit(): void {
    this.fullname = this.appServie.getName();
  }

  onClickLogout() {
    this.appServie.userLogout();
  }
}
