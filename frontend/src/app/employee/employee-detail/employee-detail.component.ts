import { AppService } from './../../app.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-employee-detail',
  templateUrl: './employee-detail.component.html',
  styleUrls: ['./employee-detail.component.css']
})
export class EmployeeDetailComponent implements OnInit {

  username: string = '';
  fullname: string = '';
  age: string = '';
  role: string = '';
  isLoading: boolean = false;

  constructor(private appService: AppService, private router: Router) { }

  ngOnInit(): void {
    this.isLoading = true;
    this.appService.getUserInfo().subscribe(res => {
      this.username = res.username;
      this.fullname = res.name;
      this.age = res.age;
      this.role = res.role;
    });
    this.isLoading = false;
  }

  onClickHome() {
    this.router.navigate(['/employee'])
  }
}
