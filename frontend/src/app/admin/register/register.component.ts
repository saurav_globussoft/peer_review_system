import { Router } from '@angular/router';
import { AdminService } from './../admin.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  showDept: boolean = false;
  isLoading: boolean = false;
  departmentList: any = [];

  constructor(private adminService: AdminService, private router: Router) { }

  ngOnInit(): void {
    this.adminService.getDepartment().subscribe(res => {
      if (typeof res.message == 'object') {
        this.departmentList = res.message;
      }
    })
  }

  userRegisterForm = new FormGroup({
    firstname: new FormControl('', { validators: [Validators.required, Validators.minLength(3)] }),
    lastname: new FormControl('', { validators: [Validators.required, Validators.minLength(3)] }),
    age: new FormControl('', Validators.required),
    username: new FormControl('', { validators: [Validators.required, Validators.minLength(6)] }),
    password: new FormControl('', { validators: [Validators.required, Validators.minLength(6)] }),
    role: new FormControl('', Validators.required),
    departmentname: new FormControl('')
  });

  onCLickEmp() {
    this.showDept = true;
  }

  onCLickAdm() {
    this.showDept = false;
  }

  onCreate() {
    if (!this.userRegisterForm.valid) {
      return;
    }
    let data = this.userRegisterForm.value;
    this.isLoading = true;
    if (!data.departmentname) {
      delete data.departmentname;
    }

    if (data.role == 'employee') {
      delete data.role;
      this.adminService.createEmployee(data).subscribe(res => {
        this.isLoading = false;
        this.router.navigate(['/admin'])
      }, err => {
        this.isLoading = false;
      });
    } else if (data.role == 'admin') {
      delete data.role;
      delete data.departmentname;
      this.adminService.createAdmin(data).subscribe(res => {
        this.isLoading = false;
        this.router.navigate(['/admin'])
      }, err => {
        this.isLoading = false;
      });
    } else {
      window.alert('Click on either admin or employee');
      this.isLoading = false;
    }
  }

  onClickHome() {
    this.router.navigate(['/admin']);
  }
}
