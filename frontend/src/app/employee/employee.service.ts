import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

const URL = 'http://localhost:3000';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor(private http: HttpClient, private router: Router) { }

  getReviews() {
    return this.http.get<{ code: number, reviewsGot: any, reviewsGiven: any }>(`${URL}/employee/review/get`);
  }

  getReviewToWhom() {
    return this.http.get<{ code: number, message: any }>(`${URL}/employee/review/review_list`);
  }

  createReview(data: any) {
    return this.http.post<{ code: number, message: any }>(`${URL}/employee/review/give`, data);
  }

  updateReview(data: any) {
    return this.http.put<{ code: number, message: any }>(`${URL}/employee/review/update`, data);
  }

  deleteReview(username: string) {
    return this.http.delete<{ code: number, message: any }>(`${URL}/employee/review/delete/${username}`);
  }
}
