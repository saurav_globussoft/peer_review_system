import { AppService } from './../app.service';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, OnDestroy {

  isUserAuthenticated: boolean = false;
  private authListenerSubs!: Subscription;

  constructor(private appService: AppService) { }

  ngOnInit(): void {
    this.isUserAuthenticated = this.appService.getAuth();
    this.authListenerSubs = this.appService.getAuthStatusListener()
    .subscribe(isAuthenticated => {
        this.isUserAuthenticated = isAuthenticated;
      });
  }

  ngOnDestroy() {
    this.authListenerSubs.unsubscribe();
  }

  onClickLogout() {
    this.appService.userLogout();
  }
}
