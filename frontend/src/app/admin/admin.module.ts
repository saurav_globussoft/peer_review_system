import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { AdminHomeComponent } from './admin-home/admin-home.component';
import { AngularMaterialModule } from '../angular-material.module';
import { AdminDetailComponent } from './admin-detail/admin-detail.component';
import { AdminUpdateComponent } from './admin-update/admin-update.component';
import { ReactiveFormsModule } from '@angular/forms';
import { RegisterComponent } from './register/register.component';
import { CreateDeptComponent } from './department/create-dept/create-dept.component';
import { DeptListComponent } from './department/dept-list/dept-list.component';
import { DeptUpdateComponent } from './department/dept-update/dept-update.component';
import { EmpListComponent } from './employee/emp-list/emp-list.component';
import { EmpUpdateComponent } from './employee/emp-update/emp-update.component';


@NgModule({
  declarations: [
    AdminHomeComponent,
    AdminDetailComponent,
    AdminUpdateComponent,
    RegisterComponent,
    CreateDeptComponent,
    DeptListComponent,
    DeptUpdateComponent,
    EmpListComponent,
    EmpUpdateComponent
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    AngularMaterialModule,
    ReactiveFormsModule
  ],
  exports: [
    AdminHomeComponent
  ]
})
export class AdminModule { }
