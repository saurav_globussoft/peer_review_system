import { AdminService } from './../../admin.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-create-dept',
  templateUrl: './create-dept.component.html',
  styleUrls: ['./create-dept.component.css']
})
export class CreateDeptComponent implements OnInit {

  isLoading: boolean = false;

  constructor(private router: Router, private adminService: AdminService) { }

  ngOnInit(): void {
  }

  departmentForm = new FormGroup({
    departmentname: new FormControl('', Validators.required)
  })

  onCreate() {
    if (!this.departmentForm.value.departmentname) {
      return;
    }
    this.isLoading = true;
    this.adminService.createDepartment(this.departmentForm.value)
      .subscribe(res => {
        window.alert(res.message);
        this.isLoading = false;
        this.router.navigate(['/admin']);
      }, err => {
        this.isLoading = false;
      });
  }

  onClickHome() {
    this.router.navigate(['/admin']);
  }
}
