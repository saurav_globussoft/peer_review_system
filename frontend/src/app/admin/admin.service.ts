import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

const URL = 'http://localhost:3000';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  constructor(private http: HttpClient, private router: Router) { }

  // update admin
  updateAdmin(data: any) {
    return this.http.put<{ code: number, message: string }>(`${URL}/admin/update`, data);
  }

  //delete admin
  deleteAdmin() {
    this.http.delete<{ code: number, message: string }>(`${URL}/admin/delete`)
      .subscribe(res => {
        localStorage.removeItem('token');
        this.router.navigate(['login']);
      }, err => {
        this.router.navigate(['login']);
      });
  }

  //create admin
  createAdmin(data: any) {
    return this.http.post<{ code: number, message: string }>(`${URL}/admin/register`, data);
  }

  //create department
  createDepartment(data: any) {
    return this.http.post<{ code: number, message: string }>(`${URL}/admin/department/create`, data);
  }

  //get department list
  getDepartment() {
    return this.http.get<{ code: number, message: any }>(`${URL}/admin/department/list`);
  }

  //update department
  updateDepartment(data: any) {
    return this.http.put<{ code: number, message: string }>(`${URL}/admin/department/update`, data);
  }

  //delete department
  deleteDepartment(departmentname: any) {
    return this.http.delete<{ code: number, message: string }>(`${URL}/admin/department/delete/${departmentname}`);
  }

  //get employee list
  getEmployee() {
    return this.http.get<{ code: number, message: any }>(`${URL}/admin/employee/list`);
  }

  //update employee
  updateEmployee(data: any) {
    return this.http.put<{ code: number, message: string }>(`${URL}/admin/employee/update`, data);
  }

  //delete employee
  deleteEmployee(username: any) {
    return this.http.delete<{ code: number, message: string }>(`${URL}/admin/employee/delete/${username}`);
  }

  //create employee
  createEmployee(data: any) {
    return this.http.post<{ code: number, message: string }>(`${URL}/admin/employee/register`, data);
  }
}
