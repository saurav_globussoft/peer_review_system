const router = require('express').Router();

const employeeRouter = require('./employee.router')

const adminRouter = require('./admin.router');

const { authenticateToken } = require('../middleware/auth.middleware');

const userModel = require('../model/user.model');

const departmentModel = require('../model/department.model')

const encryptDecrypt = require('../services/encryptdecrypt');

const { createToken } = require('../utils/jwt.util');

router.use('/admin', adminRouter);

router.use('/employee', employeeRouter);

router.post('/search', async (req, res) => {
    try {
        let { searchText } = req.body;
        if (!searchText.length) return res.status(200).json({ code: 200, message: "No search text found" });
        searchText = searchText.trim();
        if (searchText.indexOf(' ') > -1) searchText = searchText.split(' ')[0];
        let searchList = await userModel.searchUser(searchText);
        if (!searchList.length) return res.status(200).json({ code: 200, message: "No username found" });
        res.status(200).json({ code: 200, message: searchList });
    } catch (error) {
        res.status(404).json(error);
    }
});


router.get('/getUserDetail', authenticateToken, async (req, res) => {
    try {
        const { username, firstName, lastName, age, role } = req.user;
        
        res.status(200).json({
            username: username,
            name: `${firstName} ${lastName}`,
            age: age,
            role: role
        });
    } catch (error) {
        res.status(404).json(error);
    }
})

router.post('/getUserByUname', async (req, res) => {
    try {
        const { username } = req.body;
        let department = '';

        const [userData] = await userModel.getUserByUname(username);

        if (userData.department_id) {
            const departmentName = await departmentModel.getDepartmentNameById(userData.department_id);
            department = departmentName[0].department_name;
        } else {
            department = "No department"
        }

        let data = {
            username: username,
            fullname: `${userData.first_name} ${userData.last_name}`,
            department: department,
            age: userData.age,
            role: userData.role
        }
        res.status(200).json({ code: 200, message: data })
    } catch (error) {
        res.status(404).json(error);
    }
})

router.post('/login', async (req, res) => {
    try {
        const { username, password } = req.body;
   
        const [userData] = await userModel.getUserByUname(username);
        if (!userData) return res.status(404).json({ code: 404, message: "Username not found" });
        console.log("username===========", userData)
        const decryptPass = await encryptDecrypt.decryptPass(password, userData.password);
        if (!decryptPass) return res.status(401).json({ code: 401, message: "Password entered is Incorrect" });
        
        const token = createToken(userData);
        res.status(200).json({
            code: 200,
            token: token,
            role: userData.role,
            name: `${userData.first_name} ${userData.last_name}`
        });
    } catch (error) {
        res.status(404).json(error);
    }
})

module.exports = { router };