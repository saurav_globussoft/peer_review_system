import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../../employee.service';

@Component({
  selector: 'app-review-list',
  templateUrl: './review-list.component.html',
  styleUrls: ['./review-list.component.css']
})
export class ReviewListComponent implements OnInit {

  reviewGotColumns: any;
  reviewGivenColumns: any;
  showDelete: boolean = false;
  showReviewGot: boolean = false;
  showReviewGiven: boolean = false;
  isLoading: boolean = false;
  reviewsGotArr: any = [];
  reviewsGivenArr: any = [];
  uName: any;

  constructor(private empService: EmployeeService, private router: Router) { }

  ngOnInit(): void {
    this.empService.getReviews().subscribe(res => {
      if (typeof res.reviewsGot[0] == 'object') {
        let reviewsGotArr = res.reviewsGot;
        this.showReviewGot = true;
        this.reviewGotColumns = ['no', 'username', 'score', 'comment'];

        for (let i = 0; i < reviewsGotArr.length; i++) {
          reviewsGotArr[i] = {
            username: reviewsGotArr[i].reviewBy,
            score: reviewsGotArr[i].score,
            comment: reviewsGotArr[i].comment,
            no: i + 1
          }
        }
        this.reviewsGotArr = reviewsGotArr;
      }

      if (typeof res.reviewsGiven[0] == 'object') {
        let reviewsGivenArr = res.reviewsGiven;

        this.showReviewGiven = true;
        this.reviewGivenColumns = ['no', 'username', 'score', 'comment', 'edits'];

        for (let i = 0; i < reviewsGivenArr.length; i++) {
          reviewsGivenArr[i] = {
            username: reviewsGivenArr[i].giveReviewTo,
            score: reviewsGivenArr[i].score,
            comment: reviewsGivenArr[i].comment,
            no: i + 1
          }
        }
        this.reviewsGivenArr = reviewsGivenArr;
      }
    });
  }

  onDelete(username: string) {
    this.uName = username;
    this.showDelete = true;
  }

  onClickDelete() {
    this.empService.deleteReview(this.uName).subscribe(res => {
      this.router.navigate(['/employee']);
    });
  }

  onClickHide() {
    this.uName = '';
    this.showDelete = false;
  }

  onClickBack() {
    this.router.navigate(['/employee']);
  }
}
